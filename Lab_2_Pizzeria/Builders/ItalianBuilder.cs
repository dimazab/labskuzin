﻿using Lab2.Domains;
using System;

namespace Lab2.Builders
{
    sealed class ItalianBuilder
    {
        private readonly Pizza pizza = new Pizza("Italian");

        public void Cheese()
        {

            Console.WriteLine("Добавлен сыр");
            pizza.Cheese = true;
        }

        public void Tomatoes()
        {

            Console.WriteLine("Добавлены томаты");
            pizza.Tomatoes = true;
        }

        public void Olives()
        {

            Console.WriteLine("Добавлен оливки");
            pizza.Olives = true;
        }

        public void Meat()
        {

            Console.WriteLine("Добавлено мясо");
            pizza.Meat = true;
        }

        public Pizza GetPizza()
        {
            return pizza;
        }
    }
}