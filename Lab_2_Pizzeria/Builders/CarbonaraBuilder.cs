﻿using Lab2.Domains;
using System;

namespace Lab2.Builders
{
    sealed class CarbonaraBuilder
    {
        private readonly Pizza pizza = new Pizza("Carbonara");

        public void Cheese()
        {
            Console.WriteLine("Добавлен сыр");
            pizza.Cheese = true;
        }

        public void Tomatoes()
        {
            Console.WriteLine("Добавлены томаты");
            pizza.Tomatoes = true;
        }

        public void Meat()
        {
            Console.WriteLine("Добавлено мясо");
            pizza.Meat = true;
        }

        public Pizza GetPizza()
        {
            return pizza;
        }
    }
}