﻿using Lab2.Domains;
using System;

namespace Lab2.Builders
{
    sealed class MargaritaBuilder
    {
        private readonly Pizza pizza = new Pizza("Margarita");

        public void Cheese()
        {

            Console.WriteLine("Добавлен сыр");
            pizza.Cheese = true;
        }

        public void Tomatoes()
        {

            Console.WriteLine("Добавлены томаты");
            pizza.Tomatoes = true;
        }

        public void Olives()
        {

            Console.WriteLine("Добавлены оливки");
            pizza.Olives = true;
        }

        public Pizza GetPizza()
        {
            return pizza;
        }
    }
}