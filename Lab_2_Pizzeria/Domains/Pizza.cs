﻿using System;

namespace Lab2.Domains
{
    class Pizza
    {
        public string Type { get; }
        public bool Cheese { get; set; }
        public bool Tomatoes { get; set; }
        public bool Olives { get; set; }
        public bool Meat { get; set; }

        public Pizza(string type)
        {
            Type = type;
        }

        public void Show()
        {
            Console.WriteLine($"{Type}:\n"
                          + $"{(Cheese ? "\tС сыром.\n" : "")}"
                          + $"{(Tomatoes ? "\tС помидорами.\n" : "")}"
                          + $"{(Olives ? "\tС оливками.\n" : "")}"
                          + $"{(Meat ? "\tС мясом.\n" : "")}");
        }
    }
}