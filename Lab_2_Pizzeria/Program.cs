﻿using System;
using Lab2.Builders;

namespace Lab2
{
    class Program
    {
        static void Main(string[] args)
        {
            var builder = new PepperoniBuilder();
            builder.Cheese();
            builder.Tomatoes();
            builder.Meat();

            var pizza = builder.GetPizza();
            pizza.Show();

            Console.ReadLine();
        }
    }
}
