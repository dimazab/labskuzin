﻿using AbstractFactory.Factories;
using System;
using System.Collections.Generic;

namespace AbstractFactory
{
    class Program
    {
        static void Main(string[] args)
        {
            int time;
            Console.WriteLine("Current time?");
            time = Convert.ToInt32(Console.ReadLine());
            Menu factory;

            //Ужин
            factory = new Supper();

            //Завтрак
            if (time > 6 && time < 12)
            {
                factory = new Breakfast();
            }
            else if (time > 12 && time < 18)
            {
                //Обед
                factory = new Lunch();
            }

            Console.WriteLine("Current time is {0}", time);
            var b = factory.createMain();
            var r = factory.createSalad();
            var c = factory.createDesert();
            var d = factory.createDrink();
            Console.WriteLine("It's {0} with menu:", factory.GetType().Name);
            Console.WriteLine("\t {0} {1} {2} {3}", b.GetType().Name, r.GetType().Name, c.GetType().Name, d.GetType().Name);

        }
    }
}
