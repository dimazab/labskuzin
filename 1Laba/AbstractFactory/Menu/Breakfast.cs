﻿using AbstractFactory.Products;
using AbstractFactory.Products.Interface;
using AbstractFactory.Products.Linux;
using System;
using System.Collections.Generic;
using System.Text;

namespace AbstractFactory.Factories
{
    class Breakfast : Menu
    {
        public Main createMain()
        {
            return new Porridge();
        }

        public Dessert createDesert()
        {
            return new Pancake();
        }

        public Salad createSalad()
        {
            return new Eggs();
        }

        public Drink createDrink()
        {
            return new Coffe();
        }
    }
}
