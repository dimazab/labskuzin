﻿using AbstractFactory.Products;
using AbstractFactory.Products.Interface;
using AbstractFactory.Products.Windows;
using System;
using System.Collections.Generic;
using System.Text;

namespace AbstractFactory.Factories
{
    class Supper : Menu
    {
        public Main createMain()
        {
            return new Yogurt();
        }

        public Dessert createDesert()
        {
            return new Cake();
        }

        public Salad createSalad()
        {
            return new Fruit();
        }

        public Drink createDrink()
        {
            return new Tea();
        }
    }
}
