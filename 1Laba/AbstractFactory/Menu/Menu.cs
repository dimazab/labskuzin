﻿using AbstractFactory.Products;
using System;
using System.Collections.Generic;
using System.Text;
using AbstractFactory.Products.Interface;
namespace AbstractFactory.Factories
{
    interface Menu
    {
        Main createMain();
        Salad createSalad();
        Dessert createDesert();
        Drink createDrink();
    }
}
