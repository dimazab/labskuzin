﻿using AbstractFactory.Products;
using AbstractFactory.Products.Interface;
using AbstractFactory.Products.Mac;
using System;
using System.Collections.Generic;
using System.Text;

namespace AbstractFactory.Factories
{
    class Lunch : Menu
    {
        public Main createMain()
        {
           return new Pasta();
        }

        public Dessert createDesert()
        {
            return new Biscuit();
        }

        public Salad createSalad()
        {
            return new Vegies();
        }

        public Drink createDrink()
        {
            return new Compote();
        }
    }
}
